def atbash_cipher(text):
    # Визначення алфавіту
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    reversed_alphabet = alphabet[::-1]
    
    # Словник для шифрування та розшифрування
    atbash_dict = {alphabet[i]: reversed_alphabet[i] for i in range(len(alphabet))}
    
    # Виконання шифрування/розшифрування
    cipher_text = ''
    for char in text.upper():  # перетворюємо текст в верхній регістр для спрощення
        if char in atbash_dict:
            cipher_text += atbash_dict[char]
        else:
            cipher_text += char  # додаємо символ без змін, якщо він не є буквою
    
    return cipher_text

# Приклад використання
input_text = "HELLO WORLD"
ciphered = atbash_cipher(input_text)
print("Original:", input_text)
print("Ciphered:", ciphered)
